cmake_minimum_required(VERSION 3.0)
project(Calc)

# ==========================================================================
# Include things

# ==========================================================================
# Setup env
# ==========================================================================

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# ==========================================================================
# Set requirements and flags
# ==========================================================================

set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror -pedantic -g -O2")

# ==========================================================================
# Include headers
# ==========================================================================

include_directories(include)

# ==========================================================================
# Include from modules
# ==========================================================================

# include_directories(modules/)

# ==========================================================================
#  Define targets
# ==========================================================================
# set_target_properties(target PROPERTIES ...)
# target_compile_features(target features...)

# add_executable()

# add_library()

