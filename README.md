# Calc

A silly calculator

### Things that exist

Constants

    e   E  # The natural number
    pi  PI # pi

Simple arithmetic

    1 + 3 # Addition
    7 * 4 # Multiplication
    8 / 3 # Division
    9 - 7 # Subtraction

Other common operators

    7 ** 8 # Exponentiation
    7 ^  8 # Exponentiation

Common functions

    log(2, 4) # log(number, base = e)
    exp(5, 8) # exp(number, base = e)

    # Incidentiallly, these also exist
    add(1, 3)
    multiply(7, 4)
    divide(8, 3)
    subtract(9, 7)

### Miscellaneous design notes

Operators like `+`, `/`, etc are implemented behind the scenes as functions.

Variable names are bound to values. Variables and functions share a namespace.
Constants are part of the basis and are defined upon startup.

Namespaces? Const-correctness? Functions? Control Flow? Lambdas?

#### The lexer and parser

The lexer takes the program and breaks it down into tokens. A list of tokens
comprising an expression are grouped into a list. The list of these broken-down
expressions, in-toto representing the entire program, is produced by `lex`.

    lex: String -> [[Token]]

The parser takes the tokens that make up an expression and produces an AST
describing the computation that the expression represents.

    parse: [Token] -> [AST]



### An attempt at a formal grammar

    integer:= (0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9)*
            ;

    float:= integer . integer
          ;

    literal := integer
            | float
            ;

    op := + | * | - | / | ^ | **

    expression := literal op literal
                | literal
                ;

